
## Name
GOGOLE le moteur de recherche un peu foufou !

## Description
Gogole est une version parodique de Google, il se présente donc sous la forme d’un moteur de recherche classique (plus précisément dans le style d’une version un peu ancienne de Google). Seulement que lorsqu’on lui pose des questions il va nous donner des réponses pas comme les autres, c’est-à-dire qu’il va répondre d'une manière humoristique et sarcastique. C’est la raison de son nom, “GOGOLE le moteur de recherche un peu foufou”.

## Visuals 
Présentation du projet: doc/GOGOLE_2024_presentation_document.pdf

Vidéo de présentation: doc/GOGOLE_Trophee_NSI_2024.mp4

Capture d'écran: doc/Capture_ecran.png

## Installation
Pré-requis techniques pour le projet GOGOLE :

1. Python version 8.0 ou plus
2. La bibliothèque python Tkinter 
3. Accès à l'intelligence artificiel Mistral AI

Pour démarrer le projet GOGOLE lancer le fichier main.py 


## Usage
Tapez votre question dans la barre de recherche et appuyez sur enter.

## Roadmap
Nous envisageons de réaliser des pages web accessibles depuis la page d’affichage des résultats, et dont le contenu serait généré par l’IA lorsque l’on clique sur le lien.


## Authors and Contributing
Membre de l'équipe GOGOLE (trophée NSI 2024/ élèves de 1ère année 2023-2024 du Lycée Français de Barcelone) :
Adrien Dolynny-Rio, 
Alejo Toffoli, 
Gabriel Portal Palluel, 
Elena Mandl

Enseignant de NSI (superviseur du projet GOGOLE) :
Keran Le Foll

## Project status
Le projet Gogole est en cours de finalisation, nous pouvons tout de même affirmer que la liaison entre Mistral AI (intélligence artificielle française) et la page Gogole est faite. Pour tout ce qui est de Gogole, la page d'accueil est fonctionnelle ainsi que la liaison avec la page des résultats. 